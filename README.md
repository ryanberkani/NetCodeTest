# NetCodeTest

Unity NetCode example application, created from guide here: https://docs.unity3d.com/Packages/com.unity.netcode@0.50/manual/getting-started.html

## Quick Reference
- **Game.cs**: contains connection bootstrap + _GoInGame_ RPC logic for starting the game
- **CubeSpawner.cs**: simple component definition that holds a reference to our ghost prefab for the moveable cube
- **MoveableCubeComponent.cs**: the identifying component for our moveable cube ghost
- **CubeInput.cs**: the input sampling and streaming for the client control of the cube
- **MoveCube.cs**: the movement logic for the cube that handles the prediction/simulation on the client/server
- **NetCodeSample** is the main scene.